from pathlib import Path
import yaml


class AnalysisConfig:
    __slots__ = (
        "sources",
        "results",
        "bin_edges",
    )

    def __init__(self, sources: str, results: str, bin_edges: list[float]) -> None:
        self.sources = sources
        self.results = results
        self.bin_edges = bin_edges

        print("\nConfiguration loaded:")
        print(self.__repr__())

    def __repr__(self) -> str:
        return f'\nsources="{self.sources}"\nresults="{self.results}"\nbin_edges={self.bin_edges}\n'

    @property
    def project_root(self) -> Path:
        return Path(__file__).parent.parent.parent

    @property
    def file_dict(self) -> dict[str, tuple[str]]:
        fhc_files = []
        rhc_files = []

        for f in self.sources_path.iterdir():
            if "-" in f.name or "rhc" in f.name.lower():
                rhc_files.append(str(f))
            else:
                fhc_files.append(str(f))

        if len(fhc_files) == 0 and len(rhc_files) == 0:
            print(f"No files found in {self.sources}. Exiting...")

        return {"fhc": tuple(fhc_files), "rhc": tuple(rhc_files)}

    @property
    def sources_path(self) -> Path:
        if "~" in self.sources:
            return Path(self.sources).expanduser()
        return self.project_root / self.sources

    @property
    def results_path(self) -> Path:
        if "~" in self.results:
            results_path = Path(self.results).expanduser()
        else:
            results_path = self.project_root / self.results

        if not results_path.exists():
            print(f"{results_path} does not exist. Calling mkdir...")
            results_path.mkdir(parents=True)

        return results_path

    @classmethod
    def from_file(cls, config_file: str):
        with open(config_file, "r") as file:
            config = yaml.safe_load(file)
            return cls(**config)
