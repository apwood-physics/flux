from pathlib import Path
from typing import Optional

import uproot
import numpy as np
from rich.progress import track

from ROOT import TFile  # type: ignore


def ignored_reweighters_filter(th1_name: str) -> bool:
    ignored_keys = ("mipp", "pcqel", "hthin_numu", "hthin_nue", "hpot")
    return not any(x in th1_name.lower() for x in ignored_keys)


def normalize_flux_to_pot(
    input_file: str,
    output_dir: str,
    bin_edges: Optional[np.ndarray] = None,
    bin_width: bool = False,
) -> str:
    """Normalizes flux histograms to POT and saves in a new file within the specified output directory."""
    with uproot.open(input_file) as f:
        histkeys = f.keys(
            cycle=False,
            filter_classname="TH1D",
            filter_name=ignored_reweighters_filter,
        )
    input_filename = Path(input_file).name
    output_file = f"{output_dir}/{input_filename}"

    in_tfile = TFile(input_file, "read")
    out_tfile = TFile(output_file, "recreate")

    pot = in_tfile.Get("hpot").GetMaximum()

    hist_title = ";E_{#nu} (GeV);#Phi_{#nu} (m^{-2} POT^{-1})"

    for key in track(
        histkeys,
        description=f"Normalizing {input_filename} to POT and writing objects...",
    ):
        dirs, hist_name = key.rsplit("/", 1)

        if not out_tfile.Get(dirs):
            out_tfile.mkdir(dirs)

        d = out_tfile.Get(dirs)

        h = in_tfile.Get(key)
        h.SetDirectory(0)

        if bin_edges is not None:
            h = h.Rebin(len(bin_edges) - 1, h.GetName(), bin_edges)

        if bin_width:
            h.Scale(1.0 / pot, "width")
        else:
            h.Scale(1.0 / pot)

        h.SetTitle(hist_title)

        d.WriteObject(h, hist_name)

    in_tfile.Close()
    out_tfile.Close()

    return output_file
